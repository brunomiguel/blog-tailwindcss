# My personal blog port to Tailwind CSS

Based on [hugo-tailwind3-starter](https://github.com/BenjaminPrice/hugo-tailwind3-starter), a very simple starter set up with [TailwindCSS](https://tailwindcss.com/) and its [typography plugin](https://tailwindcss.com/docs/typography-plugin) and a build setup using [PostCSS](https://postcss.org/) and PurgeCSS (when running the production build).

## How to run

```bash
npm install
npm start
```
